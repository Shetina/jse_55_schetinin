package ru.t1.schetinin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.schetinin.tm.api.service.IPropertyService;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService service = propertyService;
        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + service.getApplicationName());
        System.out.println();
        System.out.println("[ABOUT]");
        System.out.println("AUTHOR: " + service.getAuthorName());
        System.out.println("E-MAIL: " + service.getAuthorEmail());
        System.out.println();
        System.out.println("[GIT]");
        System.out.println("BRANCH: " + service.getGitBranch());
        System.out.println("COMMIT ID: " + service.getGitCommitId());
        System.out.println("COMMITTER: " + service.getGitCommitterName());
        System.out.println("E-MAIL: " + service.getGitCommitterEmail());
        System.out.println("MESSAGE: " + service.getGitCommitMessage());
        System.out.println("TIME: " + service.getGitCommitTime());
    }

}