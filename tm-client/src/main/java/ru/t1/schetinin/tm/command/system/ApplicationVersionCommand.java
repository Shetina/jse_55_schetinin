package ru.t1.schetinin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.schetinin.tm.dto.request.ServerVersionRequest;
import ru.t1.schetinin.tm.dto.response.ServerVersionResponse;

@Component
public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String DESCRIPTION = "Show version info.";

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull final ServerVersionRequest request = new ServerVersionRequest();
        ServerVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println(response.getVersion());
    }

}