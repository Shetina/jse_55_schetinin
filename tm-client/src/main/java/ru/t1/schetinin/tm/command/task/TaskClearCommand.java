package ru.t1.schetinin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.schetinin.tm.dto.request.TaskClearRequest;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-clear";

    @NotNull
    private static final String DESCRIPTION = "Delete all tasks.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());
        taskEndpoint.clearTask(request);
    }

}