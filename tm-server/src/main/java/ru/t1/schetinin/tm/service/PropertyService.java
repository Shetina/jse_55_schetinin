package ru.t1.schetinin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.schetinin.tm.api.service.IPropertyService;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String DATABASE_FORMAT_SQL = "database.format_sql";

    @NotNull
    private static final String DATABASE_SECOND_LVL_CACHE = "database.second_lvl_cache";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS = "database.factory_class";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE = "database.use_query_cache";

    @NotNull
    private static final String DATABASE_USE_MIN_PUTS = "database.use_min_puts";

    @NotNull
    private static final String DATABASE_REGION_PREFIX = "database.region_prefix";

    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH = "database.config_file_path";

    @NotNull
    private static final String DATABASE_DIALECT = "database.dialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private static final String DATABASE_USERNAME = "database.username";

    @NotNull
    private static final String DATABASE_PASSWORD = "database.password";

    @NotNull
    private static final String DATABASE_URL = "database.url";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "5363453453";

    @NotNull
    private static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "10800";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "356585985";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String GIT_BRANCH = "gitBranch";

    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    public static final String GIT_COMMITTER_NAME = "gitCommitterName";

    @NotNull
    public static final String GIT_COMMITTER_EMAIL = "gitCommitterEmail";

    @NotNull
    public static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    public static final String APPLICATION_NAME_KEY = "application.name";

    @NotNull
    public static final String APPLICATION_NAME_DEFAULT = "tm";

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "application.config";

    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    public static final String APPLICATION_LOG_KEY = "application.log";

    @NotNull
    public static final String APPLICATION_LOG_DEFAULT = "./";

    @NotNull
    public static final String EMPTY_VALUE = "--//--";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }


    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationLog() {
        return getStringValue(APPLICATION_LOG_KEY, APPLICATION_LOG_DEFAULT);
    }

    @Override
    public @NotNull String getApplicationName() {
        return getStringValue(APPLICATION_NAME_KEY, APPLICATION_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @NotNull
    @Override
    public String getGitCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getGitCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @NotNull
    @Override
    public String getGitCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL);
    }

    @NotNull
    @Override
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE);
    }

    @NotNull
    @Override
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @Override
    public @NotNull String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, SESSION_TIMEOUT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUser() {
        return getStringValue(DATABASE_USERNAME, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBPassword() {
        return getStringValue(DATABASE_PASSWORD, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBUrl() {
        return getStringValue(DATABASE_URL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBDriver() {
        return getStringValue(DATABASE_DRIVER, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBDialect() {
        return getStringValue(DATABASE_DIALECT, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBShowSql() {
        return getStringValue(DATABASE_SHOW_SQL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBSecondLvlCache() {
        return getStringValue(DATABASE_SECOND_LVL_CACHE, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBFactoryClass() {
        return getStringValue(DATABASE_FACTORY_CLASS, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBUseQueryCache() {
        return getStringValue(DATABASE_USE_QUERY_CACHE, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBUseMinPuts() {
        return getStringValue(DATABASE_USE_MIN_PUTS, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBConfigFilePath() {
        return getStringValue(DATABASE_CONFIG_FILE_PATH, EMPTY_VALUE);
    }

}