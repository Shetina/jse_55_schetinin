package ru.t1.schetinin.tm.api.repository.model;

import ru.t1.schetinin.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}