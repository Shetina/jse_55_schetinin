package ru.t1.schetinin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.schetinin.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> implements ITaskDTORepository {

    @NotNull
    @Override
    public Class<TaskDTO> getEntity() {
        return TaskDTO.class;
    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) throws Exception {
        @NotNull final TaskDTO task = new TaskDTO(name, description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final TaskDTO task = new TaskDTO(name);
        return add(userId, task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, getEntity())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
