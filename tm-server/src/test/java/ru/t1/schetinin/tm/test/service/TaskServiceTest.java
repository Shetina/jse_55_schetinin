package ru.t1.schetinin.tm.test.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.service.*;
import ru.t1.schetinin.tm.api.service.dto.IProjectDTOService;
import ru.t1.schetinin.tm.api.service.dto.ITaskDTOService;
import ru.t1.schetinin.tm.api.service.dto.IUserDTOService;
import ru.t1.schetinin.tm.enumerated.Sort;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.exception.entity.TaskNotFoundException;
import ru.t1.schetinin.tm.exception.field.DescriptionEmptyException;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.exception.field.NameEmptyException;
import ru.t1.schetinin.tm.exception.user.UserIdEmptyException;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;
import ru.t1.schetinin.tm.dto.model.TaskDTO;
import ru.t1.schetinin.tm.dto.model.UserDTO;
import ru.t1.schetinin.tm.service.*;
import ru.t1.schetinin.tm.service.dto.ProjectDTOService;
import ru.t1.schetinin.tm.service.dto.TaskDTOService;
import ru.t1.schetinin.tm.service.dto.UserDTOService;
import ru.t1.schetinin.tm.test.migration.AbstractSchemeTest;
import ru.t1.schetinin.tm.util.HashUtil;

import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskServiceTest extends AbstractSchemeTest {

    @NotNull
    public final static ProjectDTO PROJECT_TEST1 = new ProjectDTO();

    @NotNull
    public final static TaskDTO TASK_TEST1 = new TaskDTO();

    @NotNull
    public final static TaskDTO TASK_TEST2 = new TaskDTO();

    @NotNull
    public final static TaskDTO TASK_TEST3 = new TaskDTO();

    @NotNull
    public final static String TASK_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE);

    @BeforeClass
    public static void setUp() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("test_login");
        @Nullable final String hash = HashUtil.salt(PropertyServiceTest.PROPERTY_SERVICE, "test_password");
        Assert.assertNotNull(hash);
        user.setPasswordHash(hash);
        USER_SERVICE.add(user);
        USER_ID = user.getId();
        PROJECT_TEST1.setName("Project_task_1");
        PROJECT_TEST1.setDescription("description_1");
        TASK_TEST1.setName("Test_task_1");
        TASK_TEST1.setDescription("description_1");
        TASK_TEST1.setProjectId(PROJECT_TEST1.getId());
        TASK_TEST2.setName("Test_task_2");
        TASK_TEST2.setDescription("description_2");
        TASK_TEST2.setProjectId(PROJECT_TEST1.getId());
        TASK_TEST3.setName("Test_task_3");
        TASK_TEST3.setDescription("description_3");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin("test_login");
        if (user != null) USER_SERVICE.remove(user);
        CONNECTION_SERVICE.close();
    }

    @Before
    public void initDemoData() throws Exception {
        PROJECT_SERVICE.add(USER_ID, PROJECT_TEST1);
        TASK_SERVICE.add(USER_ID, TASK_TEST1);
        TASK_SERVICE.add(USER_ID, TASK_TEST2);
    }

    @After
    public void clearData() throws Exception {
        TASK_SERVICE.clear(USER_ID);
        PROJECT_SERVICE.clear(USER_ID);
    }

    @Test
    public void testAddTask() throws Exception {
        Assert.assertNotNull(TASK_SERVICE.add(USER_ID, TASK_TEST3));
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(USER_ID, TASK_TEST3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_TEST3.getId(), task.getId());
    }

    @Test
    public void testExistById() throws Exception {
        @NotNull final TaskDTO task = TASK_SERVICE.findOneById(USER_ID, TASK_TEST1.getId());
        Assert.assertTrue(TASK_SERVICE.existsById(USER_ID, task.getId()));
        Assert.assertTrue(TASK_SERVICE.existsById(task.getUserId(), task.getId()));
        Assert.assertFalse(TASK_SERVICE.existsById(USER_ID, TASK_ID_FAKE));
    }

    @Test
    public void testCreateTask() throws Exception {
        @NotNull final TaskDTO task = TASK_SERVICE.create(USER_ID, TASK_TEST3.getName());
        Assert.assertEquals(USER_ID, task.getUserId());
        Assert.assertEquals(TASK_TEST3.getName(), task.getName());
    }

    @Test
    public void testCreateTaskWithDesc() throws Exception {
        @NotNull final TaskDTO task = TASK_SERVICE.create(USER_ID, TASK_TEST3.getName(), TASK_TEST3.getDescription());
        Assert.assertEquals(USER_ID, task.getUserId());
        Assert.assertEquals(TASK_TEST3.getName(), task.getName());
        Assert.assertEquals(TASK_TEST3.getDescription(), task.getDescription());
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<TaskDTO> tasks = TASK_SERVICE.findAll(USER_ID);
        Assert.assertEquals(tasks.size(), TASK_SERVICE.getSize(USER_ID));
    }

    @Test
    public void testFindAllSort() throws Exception {
        @NotNull final String sortType = "BY_CREATED";
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final List<TaskDTO> taskSort = TASK_SERVICE.findAll(USER_ID, sort.getComparator());
        Assert.assertNotNull(TASK_SERVICE.findAll(USER_ID, sort.getComparator()));
        Assert.assertEquals(TASK_SERVICE.findAll(USER_ID).toString(), taskSort.toString());
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final List<TaskDTO> tasks = TASK_SERVICE.findAll(USER_ID);
        @NotNull final TaskDTO task1 = tasks.get(0);
        @NotNull final String taskId = task1.getId();
        Assert.assertEquals(task1.toString(), TASK_SERVICE.findOneById(USER_ID, taskId).toString());
    }

    @Test
    public void testClearUser() throws Exception {
        TASK_SERVICE.clear(USER_ID);
        Assert.assertEquals(0, TASK_SERVICE.getSize(USER_ID));
    }

    @Test
    public void testRemoveById() throws Exception {
        TASK_SERVICE.removeById(USER_ID, TASK_TEST2.getId());
        Assert.assertNull(TASK_SERVICE.findOneById(USER_ID, TASK_TEST2.getId()));
    }

    @Test
    public void testRemoveByIdUserNegative() throws Exception {
        TASK_SERVICE.remove(USER_ID, TASK_TEST2);
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.removeById(USER_ID, null));
        Assert.assertThrows(TaskNotFoundException.class, () ->TASK_SERVICE.removeById("USER_ID_FAKE", TASK_TEST2.getId()));
    }

    @Test
    public void testTaskFindAllByProjectId() throws Exception {
        @NotNull final List<TaskDTO> tasks = TASK_SERVICE.findAllByProjectId(USER_ID, PROJECT_TEST1.getId());
        Assert.assertNotNull(tasks);
    }

    @Test
    public void testCreateNameNull() {
        @NotNull final String userId = USER_ID;
        @Nullable final String taskName = null;
        @NotNull final String desc = "desc";
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(userId, taskName));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(userId, taskName, desc));
    }

    @Test
    public void testCreateUserIdNull() {
        @Nullable final String userId = null;
        @NotNull final String taskName = "TaskNullId";
        @NotNull final String desc = "desc";
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(userId, taskName));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(userId, taskName, desc));
    }

    @Test
    public void testCreateDescriptionNull() {
        @NotNull final String userId = USER_ID;
        @NotNull final String taskName = "TaskNullId";
        @Nullable final String desc = null;
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.create(userId, taskName, desc));
    }

    @Test
    public void testUpdateById() throws Exception {
        @NotNull final String name = "TaskUpdate";
        @NotNull final String desc = "descUpdate";
        TASK_SERVICE.updateById(USER_ID, TASK_TEST1.getId(), name, desc);
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(USER_ID, TASK_TEST1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(desc, task.getDescription());
    }

    @Test
    public void testUpdateByIdNegative() throws Exception {
        @NotNull final String userId = USER_ID;
        @NotNull final String userIdFake = "USER_ID_FAKE";
        @NotNull final TaskDTO task = TASK_TEST1;
        @NotNull final String name = "TaskUpdate";
        @NotNull final String desc = "descUpdate";
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById(null, task.getId(), name, desc));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.updateById(userId, null, name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(userId, task.getId(), null, desc));
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.updateById(userId, task.getId(), name, null));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.updateById(userIdFake, task.getId(), name, desc));
    }

    @Test
    public void testChangeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.IN_PROGRESS;
        TASK_SERVICE.changeTaskStatusById(USER_ID, TASK_TEST1.getId(), status);
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(USER_ID, TASK_TEST1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void testChangeTaskStatusByIdNegative() throws Exception {
        @NotNull final String userId = USER_ID;
        @NotNull final String userIdFake = "USER_ID_FAKE";
        @NotNull final TaskDTO task = TASK_TEST1;
        @NotNull final Status status = Status.IN_PROGRESS;
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(null, task.getId(), status));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(userId, null, status));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.changeTaskStatusById(userIdFake, task.getId(), status));
    }

}
